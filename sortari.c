#include <stdio.h>
#include <stdlib.h>

typedef struct angajat
{
    int varsta;
    char nume[50];
    float salar;
};

void insert_sort(struct angajat a[],int n)
{
    int i,j,d;
    struct angajat k;
    for (i=1; i<n; i++)
    {
        d=i;
        k=a[i];
        for (j=0; j<i; j++)
        {
            if (a[j].varsta>a[i].varsta)
            {
                d=j;
                break;
            }
        }
        for (j=i;  j>d; j--)
        {
            a[j]=a[j-1];
        }
        a[d]=k;
    }
}

void select_sort(struct angajat a[],int n)
{
    int i,j,min;
    struct angajat d;
    for (i=0; i<n; i++)
    {
        min=i;
        for (j=i+1; j<n; j++)
        {
            if(a[j].varsta<a[min].varsta)
            {
                min=j;
            }
        }
        if(min!=i)
        {
            d=a[i];
            a[i]=a[min];
            a[min]=d;
        }
    }
}

void bubble_sort(struct angajat a[],int n)
{
    int i,j;
    struct angajat d;
    for (i=0; i<n; i++)
    {
        for (j=0; j<n-i-1; j++)
        {
            if (a[j].varsta>a[j+1].varsta)
            {
                d=a[j];
                a[j]=a[j+1];
                a[j+1]=d;
            }
        }
    }
}

void shaker_sort(struct angajat a[],int n)
{
    int i,prim,ultim,ok;
    struct angajat d;
    prim=0;
    ultim=n-1;
    ok=1;
    while(ok)
    {
        ok=0;
        for (i=prim; i<ultim; i++)
        {
            if(a[i].varsta>a[i+1].varsta)
            {
                d=a[i];
                a[i]=a[i+1];
                a[i+1]=d;
                ok=1;
            }
        }
        if (!ok)
            break;
        ok=0;
        ultim--;
        for (i=ultim-1; i>=prim; i--)
        {
            if (a[i].varsta>a[i+1].varsta)
            {
                d=a[i];
                a[i]=a[i+1];
                a[i+1]=d;
                ok=1;
            }
        }
        prim++;
    }
}

void afisare(int a[],int n)
{
    int i;
    for (i=0; i<n; i++)
    {
        printf("%d ",a[i]);
    }
    printf("\n");
}

void afisare_angajati(struct angajat v[],int n)
{
    int i;
    for (i=0; i<n; i++)
    {
        printf("%s %d %.2f",v[i].nume, v[i].varsta, v[i].salar);
        printf("\n");
    }
}

int main()
{
    struct angajat v[100];
    int a[100];
    int n,i;
    scanf("%d",&n);
    for (i=0; i<n; i++)
    {
        scanf("%s", v[i].nume);
        scanf("%d", &v[i].varsta);
        scanf("%f", &v[i].salar);
    }
    printf("afisare");
    printf("\n");
    afisare_angajati(v,n);
    printf("\n");
    insert_sort(v,n);
    printf("insert sort");
    printf("\n");
    afisare_angajati(v,n);
    printf("\n");
    select_sort(v,n);
    printf("select sort");
    printf("\n");
    afisare_angajati(v,n);
    printf("\n");
    bubble_sort(v,n);
    printf("bubble sort");
    printf("\n");
    afisare_angajati(v,n);
    printf("\n");
    shaker_sort(v,n);
    printf("shaker sort");
    printf("\n");
    afisare_angajati(v,n);
    return 0;
}
