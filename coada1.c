#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/**
First-In-First-Out == adică primul element adăugat în stivă este primul element care este scos din stivă.

Coada implementata cu tablou unidimensional.
 */

typedef struct //el de adaugat
{
  unsigned int cheie;
  int val;
}EL_t;

typedef struct //structura coada
{
  EL_t *data;
  int max_space,space_cnt;
}COADA;


COADA* init(int cap) // returneaza o coada cu capacitatea desegnata
{
  COADA *coada=malloc(sizeof(EL_t)); // creaza coada cu cap de 1 el
  if(coada==NULL)
    {
      printf("eroare");
      return NULL;
    }
  else
    {
      coada->data=malloc(cap*sizeof(EL_t)); // coada cu cap * marime el
      coada->max_space=cap;
      coada->space_cnt=0; //cate elemente sunt in coada la momentul actual
      return coada;
    }
}

COADA*  push(COADA *coada,EL_t elem)
{
  if(coada->space_cnt < coada->max_space)
    {
      coada->data[coada->space_cnt]=elem; // adauga el la noul spatiu liber
      coada->space_cnt++; // creste cnt pt ca a fost adaugat el
      return coada;
    }
  else
    {
      printf("insuficient spatiu");
      return coada;
    }

}

EL_t pop(COADA *coada)
{
  EL_t elem=coada->data[0]; // scoate primul el
  for(int i=0; i<coada->space_cnt-1; i++) // shitfeaza toate el la stanga
    {
      coada->data[i]=coada->data[i+1];
    }
  coada->space_cnt--; // scade cnt de el
  return elem;
}

void afisare(COADA *coada)
{
   printf("Afisare coada\n");
  for(int i=0; i<coada->space_cnt;i++)
    {
      printf("%d  %d \n",coada->data[i].cheie,coada->data[i].val);
    }
  printf("\n");
}


int main()
{
  COADA *coada=init(10);
  EL_t elem;
  for(int i=0; i<5; i++) //adauga el
    {
      //scanf("%d %d",&elem.cheie,&elem.val);
      elem.cheie=i;
       elem.val=i+1;
      coada=push(coada,elem);
    }
  afisare(coada);
  elem=pop(coada);
  printf("%d %d \n",elem.cheie,elem.val);
  afisare(coada);
  return 0;
}
