#include <stdio.h>
#include <stdlib.h>

/**
   First-In-First-Out == adică primul element adăugat în stivă este primul element care este scos din stivă.

   Coada implementata ca un fel de grafic cu dependenta de legatura dintre elemente(un fel de noduri)
*/


typedef struct Node //element
{
  unsigned int cheie;
  int val;
  struct Node* next; // nod urmator
} Node_t;

typedef struct
{
  Node_t *prim, *ultim; //pointer la primul si ultimul nod
} COADA;

COADA* init()
{
  COADA *coada = (COADA*)malloc(sizeof(COADA));
  coada->prim = NULL; //nu exista la init
  coada->ultim = NULL; //nu exista la init
  return coada;
}

Node_t* create_node(unsigned int cheie, int val)
{
  Node_t *new_node=(Node_t*)malloc(sizeof(Node_t));
  new_node->cheie=cheie;
  new_node->val=val;
  new_node->next=NULL;
  return new_node;
}

int is_empty(COADA *coada)
{
  return (coada->prim==NULL); //e goala daca primul nod e gol
}

void push(COADA *coada, unsigned int cheie, int val)
{
  Node_t *new_node=create_node(cheie, val);
  if (is_empty(coada))
    {
      coada->prim=new_node;
      coada->ultim=new_node;
    } else
    {
      coada->ultim->next=new_node;
      coada->ultim=new_node;
    }
}


Node_t* pop(COADA *coada)
{
  if (is_empty(coada))
    {
      printf("Coadă goală\n");
      return NULL;
    }
  Node_t *temp=coada->prim;
  coada->prim=coada->prim->next;
  if (coada->prim==NULL) //daca e scos ultimul el
    {
      coada->ultim=NULL;
    }
  return temp;
}

// Afișează elementele din coadă
void afisare(COADA *coada)
{
  if (is_empty(coada))
    {
      printf("Coadă goală\n");
      return;
    }
  Node_t *temp=coada->prim;
  printf("Afisare coada:\n");
  while (temp!=NULL)
    {
      printf("%d  %d\n", temp->cheie, temp->val);
      temp=temp->next;
    }
}

int main()
{
  COADA *coada=init();

  push(coada,1,2);
  push(coada,2,3);
  push(coada,3,4);

  afisare(coada);

  Node_t *elem=pop(coada);
  if (elem!=NULL)
    {
      printf("El scos: %d  %d\n", elem->cheie, elem->val);
      free(elem); //eliberare mem elem
    }

  afisare(coada);

  return 0;
}
