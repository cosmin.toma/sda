#include <stdio.h>
#include <stdlib.h>

/**
   First-In-Last-Out == adică primul element adăugat în stivă este ultimul element care este scos din stivă.

   Stiva implementata ca un fel de grafic cu dependenta de legatura dintre elemente(un fel de noduri)
*/


typedef struct Node //element
{
  unsigned int cheie;
  int val;
  struct Node* next; // nod urmator
} Node_t;

typedef struct
{
  Node_t *prim; //pointer la primul nod din stiva (ultimul pus)
} STIVA;

STIVA* init()
{
  STIVA *stiva = (STIVA*)malloc(sizeof(STIVA));
  stiva->prim = NULL; //nu exista la init
  return stiva;
}

Node_t* create_node(unsigned int cheie, int val)
{
  Node_t *new_node=(Node_t*)malloc(sizeof(Node_t));
  new_node->cheie=cheie;
  new_node->val=val;
  new_node->next=NULL;
  return new_node;
}
//E goala?
int is_empty(STIVA *stiva)
{
  return (stiva->prim==NULL); //e goala dc primul nod e gol
}

void push(STIVA *stiva, unsigned int cheie, int val)
{
  Node_t *new_node=create_node(cheie, val);
  if (is_empty(stiva))
    {
      stiva->prim=new_node;
    } 
 else
    {
      new_node->next = stiva->prim; // noul nod legat de cel de sub el
      stiva->prim = new_node;       // vf stivei devine noul nod
    }
}


Node_t* pop(STIVA *stiva)
{
  if (is_empty(stiva))
    {
      printf("stiva goală\n");
      return NULL;
    }
  Node_t *temp=stiva->prim; //scoatem primul el
  stiva->prim=stiva->prim->next; //vf stivei se updateaza
  return temp;
}

// Afișează elementele 
void afisare(STIVA *stiva)
{
  if (is_empty(stiva))
    {
      printf("stiva goală\n");
      return;
    }
  Node_t *temp=stiva->prim;
  printf("Afisare stiva:\n");
  while (temp!=NULL)
    {
      printf("%d  %d\n", temp->cheie, temp->val);
      temp=temp->next;
    }
}

int main()
{
  STIVA *stiva=init();
    
  push(stiva,1,2);
  push(stiva,2,3);
  push(stiva,3,4);
    
  afisare(stiva);
    
  Node_t *elem=pop(stiva);
  if (elem!=NULL)
    {
      printf("El scos: %d  %d\n", elem->cheie, elem->val);
      free(elem); //eliberare mem elem
    }

  afisare(stiva);

  return 0;
}
