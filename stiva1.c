#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/**
First-In-First-Out == adică primul element adăugat în stivă este primul element care este scos din stivă.

stiva implementata cu tablou unidimensional.
 */

typedef struct //el de adaugat
{
  unsigned int cheie;
  int val;
}EL_t;

typedef struct //structura stiva
{
  EL_t *data;
  int max_space,space_cnt;
}STIVA;


STIVA* init(int cap) // returneaza o stiva cu capacitatea desegnata
{
  STIVA *stiva=malloc(sizeof(STIVA)); // creere stiva cu cap de 1 el
  if(stiva==NULL)
    {
      printf("eroare");
      return NULL;
    }
  else
    {
      stiva->data=malloc(cap*sizeof(EL_t)); // stiva cu cap * marime el
      if(stiva->data==NULL)
        {
           printf("eroare alocare memorie");
           return NULL;
        }
      stiva->max_space=cap;
      stiva->space_cnt=0; //cate elemente sunt in stiva la momentul actual
      return stiva;
    }
}
                   
STIVA*  push(STIVA *stiva,EL_t elem)
{
  if(stiva->space_cnt < stiva->max_space)
    {
      stiva->data[stiva->space_cnt]=elem; // adauga el la noul spatiu liber
      stiva->space_cnt++; // creste cnt pt ca a fost adaugat el
    }
  else
    {
      printf("insuficient spatiu");
    }
  return stiva;
}

int is_empty(STIVA *stiva)
{
  return (stiva->space_cnt==0); //e goala dc primul nod e gol
}

EL_t pop(STIVA *stiva)
{

if(stiva->space_cnt >0)
{
 stiva->space_cnt--;
return stiva->data[stiva->space_cnt];
}
else
    {
        printf("Stiva este goală, nu se poate scoate elementul!\n");
        EL_t elem_gol = {0, 0}; // Returnează un element nul dacă stiva este goală
        return elem_gol;
    }

}

void afisare(STIVA *stiva)
{
   printf("Afisare stiva\n");
  for(int i=0; i<stiva->space_cnt;i++)
    {
      printf("%d  %d \n",stiva->data[i].cheie,stiva->data[i].val);
    }
  printf("\n");
}


int main()
{
  STIVA *stiva=init(10);
  EL_t elem;
  for(int i=0; i<5; i++) //adauga el
    {
      //scanf("%d %d",&elem.cheie,&elem.val);
      elem.cheie=i;
       elem.val=i+1;
      stiva=push(stiva,elem);
    }
  afisare(stiva);
  elem=pop(stiva);
  printf("%d %d \n",elem.cheie,elem.val);
  afisare(stiva);
 free(stiva->data);
free(stiva);
  return 0;
}

